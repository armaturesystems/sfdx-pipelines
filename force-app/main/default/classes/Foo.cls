public class Foo {

    private String name;
    public Foo(String name){
        this.name = name;
    }

    public String helloWorld(){
        return 'hello ' + name + ' from SFDX pipelines TEST DEPLOY TO STAGE';
    }

}
